INTRODUCTION
-------------------

YellowSchedule is the easiest and most powerful scheduling system in the world. 
YellowSchedule appointment booking plugin for Drupal brings an easy to integrate
appointment booking widget to Drupal for the first time. This simple, attractive
plugin places our lightweight javascript widget on your website. Allowing you to
take client bookings directly from your website 24x7.

Additional Features

* Advanced SMS text reminders (Now with client replies)
* Unique Greed Dot/Red Dot system. Easily see when clients confirm their 
  attendance with an text message.
* Securely keep client notes (CRM).
* Advanced payments system allows you to easily gather payments from clients.
* Get mobile alerts whenever you receive new appointments, client payments or 
  messages from clients.
* Track clients bookings, attendance history, payments, notes, communications.
* Ability to Synchronize with all other calendar systems Outlook/Google etc..
* Group Scheduling functionality, multi*location support, HIPAA Compliant.
* Proven system used across 64 different countries.
* Award winning product with incredibly easy to use interface.
* Get our accompanying mobile app now available for iOS, Android, Windows Phone

Security Features

* HIPAA compliance.
* SSL 128bit encryption on all data transfer.
* Client data encrypted in AES 256bit level of encryption.
* Load balanced high availability application servers for reliability and speed.
* High powered dedicated hardware protected by dedicated firewall.
* Database replication (combined with regular scheduled backups!) to protect 
  against data loss.


REQUIREMENTS
-------------------

This module requires the following modules:

* Views (https://drupal.org/project/views)
* Panels (https://drupal.org/project/panels)


INSTALLATION
-------------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules*themes/modules*7
   for further information.

* You may want to disable Toolbar module, since its output clashes with
   Administration Menu.
   

CONFIGURATION
-------------------

* Enter your business key from your account at http://www.yellowschedule.com
* Specify how many days you want to display in the widget
* Check if you would like to display name and profile picture in the module
* Save settings and you're done!

* If you do not have an account then register for a free plan at 
  http://www.yellowschedule.com


TROUBLESHOOTING
-------------------

* If the widget does not display, check the following:

Make sure you have the correct business key that can be found from within 
Booking Setup > Install code section of the YellowSchedule.com website.


FAQ
-------------------

Q: 	Is this free?
A: 	Yes. We have a complete free plan available. We also have free trials of our 
    paid plans. (Paid plans include support for more users and included text 
	message bundles for reminding clients)

Q:	Does this allow me to securely take payments from my clients.
A:	Yes, we have support for this.

Q:	What mobile platform is your app available on?
A:	Android, iOS and Windows Phone.

Q:	Are client reminders customizable.
A:	Yes, they can be easily customized to make sure that clients never forget an 
    appointment.


MAINTAINERS
-------------------

Current maintainers:
* Michael Skelly (yellowschedule) * https://www.drupal.org/u/yellowschedule
